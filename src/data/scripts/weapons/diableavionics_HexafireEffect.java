package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;

public class diableavionics_HexafireEffect implements OnHitEffectPlugin {

    private final float PITCH = 1.0f;
    private final float VOLUME = 0.2f;
    private String SOUND_ID = "diableavionics_crackle";
    
    private final String WEAPON_ID_A="diableavionics_hexafire_subA";
    private final String WEAPON_ID_B="diableavionics_hexafire_subB";
    private final String WEAPON_ID_C="diableavionics_hexafire_subC";

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        
        if (!projectile.isFading() && shieldHit){
            
            String weapon_ID=WEAPON_ID_C;
            float range = MathUtils.getDistance(point, projectile.getWeapon().getLocation())/projectile.getWeapon().getRange();
            
            if (range < (1f/3f)){
                weapon_ID=WEAPON_ID_A;
            } else if (range < (2f/3f)){                
                weapon_ID=WEAPON_ID_B;
            }
            
            float pointAngle = VectorUtils.getAngle(point, target.getShield().getLocation());
            if (Math.random()<(Math.abs(MathUtils.getShortestRotation(VectorUtils.getFacing(projectile.getVelocity()), pointAngle)/45)*0.8f+0.2f)){
                float bounceAngle = pointAngle + 180 + MathUtils.getShortestRotation(VectorUtils.getFacing(projectile.getVelocity()), pointAngle);

                engine.spawnProjectile(
                        projectile.getSource(),
                        projectile.getWeapon(),
                        weapon_ID,
                        point,
                        bounceAngle,
                        target.getVelocity()
                );
                if(Math.random()<0.25f){
                    Global.getSoundPlayer().playSound(SOUND_ID, PITCH*(float)((Math.random()*0.2f)+0.8f), VOLUME*(float)((Math.random()*0.2f)+0.8f),target.getLocation(), target.getVelocity());
                }
            } 
        }
    }
}