package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEngineLayers;
import com.fs.starfarer.api.combat.CombatEntityAPI;
//import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
//import com.fs.starfarer.api.combat.ShipAPI;
//import data.scripts.plugins.DiableAvionics_projectileEffectPlugin;
//import data.scripts.util.MagicLensFlare;
import data.scripts.util.MagicRender;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
//import org.lazywizard.lazylib.combat.entities.SimpleEntity;

public class Diableavionics_banishEffect implements OnHitEffectPlugin {

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        
//        if (!projectile.isFading() && shieldHit){
//            
//            //BIG shield disruption
//            DiableAvionics_projectileEffectPlugin.addDisruption((ShipAPI)target, 2f);
//            
//        }

        if(MagicRender.screenCheck(0.25f, projectile.getLocation())){
            
            MagicRender.battlespace(
                    Global.getSettings().getSprite("fx","banishSharp"),
                    new Vector2f(point),
                    new Vector2f(),
                    new Vector2f(128,128),
                    new Vector2f(-384,-384), 
                    MathUtils.getRandomNumberInRange(0, 360), 
                    MathUtils.getRandomNumberInRange(-1, 1), 
                    Color.PINK, 
                    false,
                    0,
                    0.1f,
                    0.2f,
                    CombatEngineLayers.JUST_BELOW_WIDGETS
            );
            MagicRender.battlespace(
                    Global.getSettings().getSprite("fx","banishDiffuse"),
                    new Vector2f(point),
                    new Vector2f(),
                    new Vector2f(194,194),
                    new Vector2f(-196,-196), 
                    MathUtils.getRandomNumberInRange(0, 360), 
                    MathUtils.getRandomNumberInRange(-1, 1),
                    Color.BLUE, 
                    false,
                    0f,
                    0.15f,
                    0.45f,
                    CombatEngineLayers.JUST_BELOW_WIDGETS
            );
            MagicRender.battlespace(
                    Global.getSettings().getSprite("fx","banishDiffuse"),
                    new Vector2f(point),
                    new Vector2f(),
                    new Vector2f(156,156),
                    new Vector2f(-128,-128), 
                    MathUtils.getRandomNumberInRange(0, 360), 
                    MathUtils.getRandomNumberInRange(-1, 1),
                    Color.WHITE, 
                    false,
                    0.2f,
                    0.05f,
                    0.35f,
                    CombatEngineLayers.JUST_BELOW_WIDGETS
            );
            
            MagicRender.battlespace(
                    Global.getSettings().getSprite("fx","banishFlash"),
                    new Vector2f(point),
                    new Vector2f(),
                    new Vector2f(128,128),
                    new Vector2f(), 
                    MathUtils.getRandomNumberInRange(0, 360), 
                    MathUtils.getRandomNumberInRange(-1, 1), 
                    Color.WHITE, 
                    true,
                    0,
                    0.05f,
                    0.05f,
                    CombatEngineLayers.JUST_BELOW_WIDGETS
            );
            
            for(int i=0; i<MathUtils.getRandomNumberInRange(4, 8); i++){
                
                int size = MathUtils.getRandomNumberInRange(16, 54);
                float fade = MathUtils.getRandomNumberInRange(0.15f, 0.5f);
                CombatEngineLayers layer = CombatEngineLayers.JUST_BELOW_WIDGETS;
                if(Math.random()<fade){
                    layer = CombatEngineLayers.BELOW_INDICATORS_LAYER;
                }
                
                MagicRender.battlespace(
                        Global.getSettings().getSprite("fx","banishSharp"),
                        MathUtils.getRandomPointOnCircumference(
                                point,
                                MathUtils.getRandomNumberInRange(32, 128-size)
                        ),
                        new Vector2f(),
                        new Vector2f(size,size),
                        new Vector2f(-size/fade,-size/fade), 
                        MathUtils.getRandomNumberInRange(0, 360), 
                        MathUtils.getRandomNumberInRange(-1, 1), 
                        new Color(128,24,200,255), 
                        false,
                        0,
                        3*fade/4,
                        fade/4,
                        layer
                );
                
            }
            
//            //visual effect
//            int offset = MathUtils.getRandomNumberInRange(20, 60);
//            boolean closed=false;
//            Vector2f pointA = MathUtils.getPoint(point, MathUtils.getRandomNumberInRange(60, 120), projectile.getFacing());
//            Vector2f pointB = MathUtils.getPoint(point, MathUtils.getRandomNumberInRange(60, 120), projectile.getFacing()+offset);
//
//            while (!closed){
//                engine.spawnEmpArc(
//                        projectile.getSource(),
//                        pointA, 
//                        new SimpleEntity(point),
//                        new SimpleEntity(pointB),
//                        DamageType.KINETIC,
//                        0,
//                        0, 
//                        200, 
//                        null,
//                        MathUtils.getRandomNumberInRange(3, 6),
//                        new Color(150,20,200,32), 
//                        new Color(50,10,150,90)
//                );
//                offset+= MathUtils.getRandomNumberInRange(20, 60);
//                if(offset>=360){
//                    offset=360;
//                    closed=true;
//                }
//                pointA=pointB;
//                pointB=MathUtils.getPoint(point, MathUtils.getRandomNumberInRange(60, 120), projectile.getFacing()+offset);
//            }
//
//            engine.addSmoothParticle(point, new Vector2f(), 400, 5, 0.15f, Color.WHITE);
//
//            for (int i=0; i<MathUtils.getRandomNumberInRange(4, 8); i++){
//                MagicLensFlare.createSharpFlare(
//                        engine,
//                        projectile.getSource(),
//                        MathUtils.getRandomPointInCircle(point, 100), 
//                        6, 
//                        400, 
//                        0,
//                        new Color(150,20,200,100), 
//                        new Color(50,10,150,100)
//                );
//            }
        }
        
        Global.getSoundPlayer().playSound(
                "diableavionics_banish_blast",
                1f,
                1,
                point,
                target.getLocation()
        );
        
    }
}