package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class diableavionics_glowtuskEffect implements OnHitEffectPlugin {

    private static final Color EXPLOSION_COLOR = new Color(255, 145, 72, 255);
    private static final float MAX_EXTRA_DAMAGE = 100f;
    private static final float MIN_EXTRA_DAMAGE = 500f;
    private static final int NUM_PARTICLES = 14;
    private static final Color PARTICLE_COLOR = new Color(255, 145, 72, 255);
    private static final String SOUND_ID = "diableavionics_glowtusk_effect";

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        
        if(shieldHit){
            engine.applyDamage(target, point, MathUtils.getRandomNumberInRange(MIN_EXTRA_DAMAGE, MAX_EXTRA_DAMAGE), DamageType.FRAGMENTATION, 0f, false, true, projectile.getSource());
        }

        // Spawn visual effects
        Vector2f vel = new Vector2f(target.getVelocity());
        vel.scale(0.45f);
        engine.spawnExplosion(point, vel, EXPLOSION_COLOR, 155f, 0.44f);
        float speed = projectile.getVelocity().length();
        float facing = projectile.getFacing();
        for (int x = 0; x < NUM_PARTICLES; x++) {
            engine.addHitParticle(
                    point, 
                    MathUtils.getPointOnCircumference(
                            null,
                            MathUtils.getRandomNumberInRange(speed * .009f, speed * .33f),
                            MathUtils.getRandomNumberInRange(facing - 160f, facing + 160f)),
                    7f,
                    1f,
                    1.6f,
                    PARTICLE_COLOR);
        }
        // Spawn sound
        Global.getSoundPlayer().playSound(SOUND_ID, 1f, 0.5f, target.getLocation(), target.getVelocity());        
    }
}
