package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;

public class diableavionics_UhlanFire implements EveryFrameWeaponEffectPlugin {
        
    private boolean runOnce=false, sound=false, hidden=false;
    private SpriteAPI barrel;
    private float barrelHeight=0, recoil=0, ammo=0;
    private final float maxRecoil=-15;
    
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(engine.isPaused() || weapon.getShip().getOriginalOwner()==-1){return;}
        
        if(!runOnce){
            runOnce=true;
            if(weapon.getSlot().isHidden()){
                hidden=true;
            } else {
                barrel=weapon.getBarrelSpriteAPI();
                if(weapon.getSlot().isTurret()){
                    barrelHeight=barrel.getHeight()/2;
                } else {                    
                    barrelHeight=barrel.getHeight()/4;
                }
            }
            return;
        }
        
        if(!hidden){
            if(weapon.getChargeLevel()==1 && weapon.getAmmo()<ammo){
                recoil=Math.min(1, recoil+0.33f);
                engine.addHitParticle(
                        MathUtils.getPointOnCircumference(weapon.getLocation(), (20-recoil*maxRecoil), weapon.getCurrAngle()),
                        weapon.getShip().getVelocity(),
                        150,
                        1,
                        0.5f,
                        Color.red
                );
            } else {
                recoil=Math.max(0, recoil-(0.2f*amount));
            }
            barrel.setCenterY(barrelHeight-(recoil*maxRecoil));
            ammo=weapon.getAmmo();            
        }
        
        if(sound && weapon.getChargeLevel()<1){
            Global.getSoundPlayer().playSound("diableavionics_uhlan_chargedown", 1, 1, weapon.getLocation(), weapon.getShip().getVelocity());
            sound=false;
        } else if (weapon.getChargeLevel()==1){
            sound=true;
        }
    }
}