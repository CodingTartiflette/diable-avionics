package data.scripts.weapons;
import com.fs.starfarer.api.Global;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;

public class diableavionics_UhlanEffect implements OnHitEffectPlugin {

    private Vector2f particleVelocity1;
    private Vector2f particleVelocity2;
    private Color particleColor = new Color(255,58,58,225);
    private float particleSize = 3f;
    private float particleBrightness = 1.48f;
    private float particleDuration = 1.75f;
    private float explosionSize = 360f;
    private float explosionSize2 = 150f;
    private float explosionDuration = 0.33f;
    private float explosionDuration2 = 1.4f;
    private String soundName = "diableavionics_explosionA";

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        particleVelocity1 = new Vector2f(projectile.getVelocity());
        particleVelocity2 = new Vector2f(projectile.getVelocity());
        particleVelocity1.scale(0.05f);
        particleVelocity2.scale(0.1f);
        engine.addHitParticle(point, particleVelocity1, particleSize, particleBrightness, particleDuration, particleColor);
        engine.addHitParticle(point, particleVelocity2, particleSize, particleBrightness, particleDuration, particleColor);					
        engine.spawnExplosion(point, particleVelocity1, particleColor, explosionSize, explosionDuration);
        engine.spawnExplosion(point, particleVelocity2, particleColor, explosionSize2, explosionDuration2);					

        Global.getSoundPlayer().playSound(soundName, 1f, 1f,target.getLocation(), target.getVelocity());
    }
}