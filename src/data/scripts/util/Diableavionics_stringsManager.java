/*
By Tartiflette
 */
package data.scripts.util;

import com.fs.starfarer.api.Global;

public class Diableavionics_stringsManager {    
//    
//    private static final String FILE = "data/strings/diableavionics_strings.csv";
//    private static final Logger LOG = Global.getLogger(Diableavionics_stringsManager.class); 
//    private static final Map<String, String> DA_STRINGS=new HashMap<>();
//    
//    public static void readStringsFile(){
//        DA_STRINGS.clear();
//        try {
//            JSONArray StringData = Global.getSettings().getMergedSpreadsheetDataForMod("id", FILE, "diableavionics");
//            for(int i = 0; i < StringData.length(); i++) {
//                JSONObject row = StringData.getJSONObject(i);
//                if(row.getString("id")!=null){
//                    DA_STRINGS.put(row.getString("id"), row.getString("text"));
//                }
//            }
//        } catch (IOException | JSONException ex) {
//            LOG.error("unable to read "+FILE);
//        }
//    }
//    
//    public static String getString(String id){
//        return DA_STRINGS.get(id);
//    }    
    private static final String ML="diableavionics";    
    
    public static String getString(String id){
        return Global.getSettings().getString(ML, id);
    }       
}