package data.scripts.world;

import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import data.scripts.world.systems.Diableavionics_stagging;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.world.systems.Diableavionics_fob;
import data.scripts.world.systems.Diableavionics_outerTerminus;

@SuppressWarnings("unchecked")
public class diableavionicsGen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {
	
        new Diableavionics_outerTerminus().generate(sector);
        new Diableavionics_stagging().generate(sector);
        new Diableavionics_fob().generate(sector);

        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("diableavionics");                      

        FactionAPI diableavionics = sector.getFaction("diableavionics");
        FactionAPI player = sector.getFaction(Factions.PLAYER);
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI tritachyon = sector.getFaction(Factions.TRITACHYON);
        FactionAPI pirates = sector.getFaction(Factions.PIRATES);
        FactionAPI independent = sector.getFaction(Factions.INDEPENDENT); 
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);   	
        FactionAPI diktat = sector.getFaction(Factions.DIKTAT); 
        FactionAPI kol = sector.getFaction(Factions.KOL);	
	FactionAPI persean = sector.getFaction(Factions.PERSEAN);
        FactionAPI guard = sector.getFaction(Factions.LIONS_GUARD);
        FactionAPI remnant = sector.getFaction(Factions.REMNANTS);
        FactionAPI derelict = sector.getFaction(Factions.DERELICT);

        diableavionics.setRelationship(player.getId(), -0.1f);	
        diableavionics.setRelationship(hegemony.getId(), -0.51f);
        diableavionics.setRelationship(tritachyon.getId(), -0.1f);
        diableavionics.setRelationship(pirates.getId(), 0.55f);
        diableavionics.setRelationship(independent.getId(), -0.2f);	
        diableavionics.setRelationship(church.getId(), -0.3f);
        diableavionics.setRelationship(path.getId(), 0.0f);    
        diableavionics.setRelationship(diktat.getId(), -0.45f);      
        diableavionics.setRelationship(guard.getId(), -0.55f);  
        diableavionics.setRelationship(persean.getId(), -0.15f);	
        diableavionics.setRelationship(kol.getId(), -0.05f);     
        
        diableavionics.setRelationship(remnant.getId(), -0.5f);	
        diableavionics.setRelationship(derelict.getId(), 0.5f);     
        
        
        diableavionics.setRelationship("syndicate_asp", -0.1f);	     
        diableavionics.setRelationship("junk_pirates", 0.15f); 
        diableavionics.setRelationship("pack", 0.0f);          
        
        diableavionics.setRelationship("exigency", -0.2f);	
        diableavionics.setRelationship("exipirated", 0.2f);	 
        
        diableavionics.setRelationship("shadow_industry", -0.55f);	 
        diableavionics.setRelationship("pirateAnar", 0.3f);	
            
        diableavionics.setRelationship("sun_ice", -0.25f);     
        diableavionics.setRelationship("sun_ici", 0.15f);     
        
        diableavionics.setRelationship("mayorate", 0.1f);	              
        diableavionics.setRelationship("blackrock_driveyards", -0.75f);	    
        diableavionics.setRelationship("tiandong", -0.2f);        
        diableavionics.setRelationship("SCY", -0.2f);      
        diableavionics.setRelationship("ORA", -0.2f);     
        diableavionics.setRelationship("neutrinocorp", -0.15f); 
        diableavionics.setRelationship("interstellarimperium", -0.25f);         
        diableavionics.setRelationship("citadeldefenders", -0.35f);	
        diableavionics.setRelationship("mayorate", 0.1f);      
        diableavionics.setRelationship("pn_colony", -0.2f);        
        diableavionics.setRelationship("crystanite", -0.3f); 
        
        diableavionics.setRelationship("cabal", 0.4f);     
        diableavionics.setRelationship("Coalition", -0.2f);      
        diableavionics.setRelationship("metelson", -0.2f);     
        diableavionics.setRelationship("the_deserter", 0.35f); 
        diableavionics.setRelationship("blade_breakers", 0.25f);         
        diableavionics.setRelationship("6eme_bureau", 0.0f);	
        diableavionics.setRelationship("dassault_mikoyan", -0.25f);
                
        diableavionics.setRelationship("noir", 0.0f);     
        diableavionics.setRelationship("Lte", 0.0f);  
        diableavionics.setRelationship("GKSec", 0.1f); 
        diableavionics.setRelationship("gmda", -0.1f);   
        diableavionics.setRelationship("oculus", -0.25f);     
        diableavionics.setRelationship("nomads", -0.25f); 
        diableavionics.setRelationship("thulelegacy", -0.25f); 
        diableavionics.setRelationship("infected", -0.99f);        
        diableavionics.setRelationship("new_galactic_order", -0.99f);	
        diableavionics.setRelationship("explorer_society", -0.99f);
    }
}
