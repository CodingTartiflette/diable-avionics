package data.hullmods;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.BaseHullMod;
import static data.scripts.util.Diableavionics_stringsManager.getString;

public class DiableAvionicsBurst extends BaseHullMod {    
    
    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if (index == 0) return getString("hm_selector_1");
        if (index == 1) return getString("hm_selector");        
        return null;
    }
}
