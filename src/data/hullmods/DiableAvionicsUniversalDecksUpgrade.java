package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.ui.Alignment;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import static data.scripts.util.Diableavionics_stringsManager.getString;
import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DiableAvionicsUniversalDecksUpgrade extends BaseHullMod {  
    
    private final float REFIT_BONUS=50;
    private final float REPLACEMENT_DECREASE_MODIFIER = 20f;        
    private final Map<String, Float> BASE_VALUES = new HashMap<>();    
    private final String PATH= "data/hulls/wanzers_data.csv";
    
    private final Logger log = Global.getLogger(DiableAvionicsUniversalDecksUpgrade.class);
    
    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return (int) REFIT_BONUS+getString("%");
        }
        if (index == 1) {
            return (int) REPLACEMENT_DECREASE_MODIFIER+getString("%");
        }        
        return null;
    }
    
    private final String POST0=getString("hm_gantry_0");
    private final String POST1=getString("hm_gantry_1");
    private final String POST2=getString("hm_gantry_2");
    private final String POST3=getString("hm_gantry_3");
    private final String POST4=getString("hm_gantry_4");
    private final String POST5=getString("hm_gantry_5");
    private final String POST6=getString("hm_gantry_6");
    private final String POST7=getString("hm_gantry_7");
    private final String POST8=getString("hm_gantry_8");
    private final Color HL=Global.getSettings().getColor("hColor");
    
    @Override
    public void addPostDescriptionSection(TooltipMakerAPI tooltip, ShipAPI.HullSize hullSize, ShipAPI ship, float width, boolean isForModSpec) {
        //title
        tooltip.addSectionHeading(POST0, Alignment.MID, 15);        
        
        if(ship!=null && ship.getVariant()!=null){
            if( ship.getVariant().getNonBuiltInWings().isEmpty()){
                //no wing fitted
                tooltip.addPara(
                        POST1
                        ,10
                        ,HL
                );
            } else if(!allWanzers(ship.getVariant())){
                //non wanzer wings installed
                tooltip.addPara(
                        POST2
                        ,10
                        ,HL
                );
            } else {
                //effect applied
                String depletion = String.valueOf((int)(REPLACEMENT_DECREASE_MODIFIER*ship.getVariant().getNonBuiltInWings().size()));
                String wings = String.valueOf((int)ship.getVariant().getNonBuiltInWings().size());
                tooltip.addPara(
                        POST3
                        + depletion
                        + POST4
                        + wings
                        + POST5
                        ,10
                        ,HL
                        ,depletion
                        ,wings
                );

                //list new wanzer replacement rates
                tooltip.addPara(
                        POST6
                        ,10
                        ,HL
                );

                tooltip.setBulletedListMode("    - ");  

                for(String w : ship.getVariant().getNonBuiltInWings()){
                    String wingName = Global.getSettings().getFighterWingSpec(w).getWingName();
                    int newTime = (int)Global.getSettings().getFighterWingSpec(w).getRefitTime()/2;

                    tooltip.addPara(
                            wingName
                            + POST7
                            + newTime
                            + POST8
                            ,3
                            ,HL
                            ,""+newTime
                    );
                }
                tooltip.setBulletedListMode(null);
            }
        }
        
    }
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        
        if(BASE_VALUES.isEmpty()){
            try{                
                JSONArray wanzer = Global.getSettings().getMergedSpreadsheetDataForMod("id", PATH, "diableavionics");
            
                for(int i = 0; i < wanzer.length(); i++) {            
                    JSONObject row = wanzer.getJSONObject(i);
                    BASE_VALUES.put(row.getString("id"), (float)row.getDouble("time"));
                }
            } catch (IOException | JSONException ex) {
                log.error("unable to read wanzers_data.csv");
                return;
            }
        }
        
        boolean all_wanzers=allWanzers(stats.getVariant());
        
        if(!all_wanzers){
            log.info("WANZER GANTRY - Non wanzer wing found, no effect");
        } else {
            log.info("WANZER GANTRY - All wanzer complement, reducing refit time.");
            
            //faster repairs
            stats.getFighterRefitTimeMult().modifyMult("wanzer_gantry", 1-(REFIT_BONUS/100), "Wanzer Gantry bonus");
            //faster depletion of replacement rate
            float depletion = REPLACEMENT_DECREASE_MODIFIER*stats.getVariant().getNonBuiltInWings().size();
            stats.getDynamic().getStat(Stats.REPLACEMENT_RATE_DECREASE_MULT).modifyPercent(id, depletion);
            //debug
//            ship.getMutableStats().getFighterRefitTimeMult().modifyMult("wanzer_gantry", 0.01f, "Wanzer Gantry bonus");
        }
    }
    
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        if(ship==null) return false;
        int bays = (int) ship.getMutableStats().getNumFighterBays().getBaseValue();
        return bays > 0; 
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        return getString("hm_noBays");
    }

    private boolean allWanzers(ShipVariantAPI v){
        boolean all_wanzers=true;
        for(String w : v.getWings()){
            if (!w.equals("") && (!w.startsWith("diable")||!BASE_VALUES.containsKey(w))){
                all_wanzers=false;
            }
        }
        return all_wanzers;
    }
}
