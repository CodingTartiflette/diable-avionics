package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class Diableavionics_transit2Stats extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (state == ShipSystemStatsScript.State.OUT) {
            stats.getMaxSpeed().unmodify(id);
        } else {
            stats.getMaxSpeed().modifyFlat(id, 130f * effectLevel);
            stats.getDeceleration().modifyPercent(id, -90f * effectLevel);
            stats.getTurnAcceleration().modifyPercent(id, -75f * effectLevel);
            stats.getMaxTurnRate().modifyPercent(id, -25f * effectLevel);
            stats.getArmorDamageTakenMult().modifyPercent(id, -50f * effectLevel);
            stats.getHullDamageTakenMult().modifyPercent(id, -25f * effectLevel);
        }
    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().unmodify(id);
        stats.getDeceleration().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);        
        stats.getArmorDamageTakenMult().unmodify(id);
        stats.getHullDamageTakenMult().unmodify(id);
    }
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
                return new StatusData("-"+Math.round(75f * effectLevel)+"% maneuverability", false);
        }
        if (index == 1) {
                return new StatusData("+"+Math.round(100f * effectLevel)+" top speed", false);
        }
        if (index == 2) {
                return new StatusData("-"+Math.round(25f * effectLevel)+"% damage taken", false);
        }
        return null;
    }
}
