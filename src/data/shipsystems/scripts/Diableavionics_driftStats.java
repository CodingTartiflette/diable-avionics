package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
//import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import data.scripts.util.MagicAnim;
import java.awt.Color;

public class Diableavionics_driftStats extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        
        float effect = Math.min(1, Math.max(0, MagicAnim.RSO(effectLevel, 0, 1)/2 + MagicAnim.RSO(effectLevel*1.5f, 0, 1)/2 + MagicAnim.RSO(effectLevel*2, 0, 1)/2));
        
        //visual effect
        ShipAPI ship = (ShipAPI) stats.getEntity();
        if(ship!=null){
            ship.setJitterUnder(
                    ship, 
                    Color.CYAN,
                    0.5f*effect,
                    5, 
                    5+5f*effect, 
                    5+10f*effect
            );
            if(Math.random()>0.9f){
                ship.addAfterimage(new Color(0,200,255,64), 0, 0, -ship.getVelocity().x, -ship.getVelocity().y, 5+50*effect, 0, 0, 2*effect, false, false, false);
            }  
            
            if(!stats.getTimeMult().getPercentMods().containsKey(id)){
                Global.getSoundPlayer().playSound("diableavionics_drift", 1, 1.66f, ship.getLocation(), ship.getVelocity());
                
                
                //protection against burst weapons abuse
                ship.setPhased(true);
//                for (WeaponAPI w : ship.getAllWeapons()){
//                    if(w.getChargeLevel()==1){
//                        w.setRemainingCooldownTo(w.getCooldown());
//                    }
//                }
            } else if(ship.isPhased()){
                ship.setPhased(false);
            }
        }
        
        //ship can reorient
        stats.getTurnAcceleration().modifyPercent(id, 1000f * effect);
        stats.getMaxTurnRate().modifyPercent(id, 500f*effect);
        
        //ship can slightly jump forward
        stats.getMaxSpeed().modifyPercent(id, 200f*effect);
        stats.getAcceleration().modifyPercent(id, 200f);
        stats.getDeceleration().modifyPercent(id, 200f);
        
        //time drift
        stats.getTimeMult().modifyPercent(id, 1000f*effect);
        
        
    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxTurnRate().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        
        stats.getMaxSpeed().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
        
        stats.getTimeMult().unmodify(id);
    }
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("Phase Drift in progress", false);
        }
        return null;
    }
}