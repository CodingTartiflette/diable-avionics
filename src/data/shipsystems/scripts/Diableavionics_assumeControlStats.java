package data.shipsystems.scripts;

import java.awt.Color;
import java.util.EnumSet;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.util.Misc;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.util.ArrayList;
import java.util.List;

public class Diableavionics_assumeControlStats extends BaseShipSystemScript {
    
    public static final Color JITTER_UNDER_COLOR = new Color(150,100,50,50);
    public static final float MAX_TIME_MULT = 1.25f;
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        ShipAPI ship;
        if (stats.getEntity() instanceof ShipAPI) {
                ship = (ShipAPI) stats.getEntity();
        } else {
                return;
        }
        
        if (effectLevel > 0) {
//            float jitterLevel = effectLevel;
            
            for (ShipAPI fighter : getFighters(ship)) {
                
                if (fighter.isHulk()) continue;
                
                //accelerate fighters
                float shipTimeMult = 1f + (MAX_TIME_MULT - 1f) * effectLevel;
		fighter.getMutableStats().getTimeMult().modifyMult(id, shipTimeMult);
                //visual effect
                if (effectLevel > 0) {
                    fighter.setWeaponGlow(effectLevel, Misc.setAlpha(JITTER_UNDER_COLOR, 50), EnumSet.allOf(WeaponType.class));
                    fighter.setJitterUnder(fighter, JITTER_UNDER_COLOR, effectLevel, (int)(5*effectLevel), 3, 3+3*effectLevel);
                }               
            }            
        }                
    }

    private List<ShipAPI> getFighters(ShipAPI carrier) {
        List<ShipAPI> result = new ArrayList<>();
        for (ShipAPI ship : Global.getCombatEngine().getShips()) {
            if (!ship.isFighter()) continue;
            if (ship.getWing() == null) continue;
            if (ship.getWing().getSourceShip() == carrier) {
                result.add(ship);
            }
        }		
        return result;
    }
    
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship;
        
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();                
        } else {
            return;
        }

        for (ShipAPI fighter : getFighters(ship)) {
            if (fighter.isHulk()) continue;
            fighter.setWeaponGlow(0, Color.BLACK, EnumSet.allOf(WeaponType.class));
            fighter.getMutableStats().getTimeMult().unmodify(id);
        }
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        if (index == 0) {
            return new ShipSystemStatsScript.StatusData("Improved fighter performances.", false);
        }
        return null;
    }
}