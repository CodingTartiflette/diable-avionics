package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class Diableavionics_boosterStats extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        
        stats.getTurnAcceleration().modifyMult(id, 0.5f);
        stats.getMaxTurnRate().modifyMult(id, 0.25f);
        
        stats.getMaxSpeed().modifyFlat(id, 150f);
        
        stats.getAcceleration().modifyMult(id, 2);
        stats.getDeceleration().modifyMult(id, 0.1f);     
        
        stats.getBallisticWeaponRangeBonus().modifyMult(id, 0.01f);
    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        
        stats.getMaxSpeed().unmodify(id);
        
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
        
        stats.getBallisticWeaponRangeBonus().unmodify(id);
    }
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
                return new StatusData("+150 top speed", false);
        }
        return null;
    }
}
