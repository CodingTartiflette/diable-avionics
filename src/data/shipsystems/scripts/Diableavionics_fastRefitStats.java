package data.shipsystems.scripts;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class Diableavionics_fastRefitStats extends BaseShipSystemScript {

    private final float BOOST=50, CONSUMPTION=100;	

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if(effectLevel>0){            
            stats.getFighterRefitTimeMult().modifyPercent(id, BOOST*effectLevel);
            stats.getDynamic().getStat(Stats.REPLACEMENT_RATE_DECREASE_MULT).modifyPercent(id, CONSUMPTION*effectLevel);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {        
        stats.getFighterRefitTimeMult().unmodify(id);
        stats.getDynamic().getStat(Stats.REPLACEMENT_RATE_DECREASE_MULT).unmodify(id);
    }	

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("-"+(int)(BOOST * effectLevel)+"% fighters replacement time.", false);
        }
        if (index == 1) {
            return new StatusData("+"+(int)(CONSUMPTION * effectLevel)+"% carrier replacement rate usage.", true);
        }
        return null;
    }
}